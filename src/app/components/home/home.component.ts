import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUbigeo } from 'src/app/models/ubigeo.interface';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit, OnDestroy {

  getDataSub: Subscription;

  ubigeos: Array<IUbigeo> = [];
  deparment: Array<IUbigeo> = [];
  province: Array<IUbigeo> = [];
  distric: Array<IUbigeo> = [];

  constructor(private ubigeoService: UbigeoService) { }

  ngOnInit(): void {
    this.getDataSub = this.ubigeoService.getData()
      .subscribe(ubigeos => {
        this.deparment = ubigeos.filter(ubigeo => !ubigeo.provinceCode);
        this.province = ubigeos.filter(ubigeo => ubigeo.provinceCode && !ubigeo.districtCode);
        this.distric = ubigeos.filter(ubigeo => ubigeo.districtCode);

        this.ubigeos = [].concat(this.deparment, this.province, this.distric);
        this.getDataSub.unsubscribe();
      }, err => this.getDataSub.unsubscribe());
  }

  ngOnDestroy(): void {
    if (this.getDataSub) { this.getDataSub.unsubscribe(); }
  }

}
