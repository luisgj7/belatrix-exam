import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUbigeo } from '../models/ubigeo.interface';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  constructor(private http: HttpClient) {
  }

  getData(): Observable<Array<IUbigeo>> {
    return this.http.get('./assets/input.txt', { responseType: 'text' })
      .pipe(map(text => {
        const ubigeos: Array<IUbigeo> = [];

        let rows = text.split('\n');
        for (let i = 0; i < rows.length; i++) {
          let rowAux = rows[i];
          let rowDetail = rowAux.split('/');
          if (rowDetail.length == 3) {
            let departmentData = this.getCodeName(rowDetail[0]);
            let provinceData = this.getCodeName(rowDetail[1]);
            let districtData = this.getCodeName(rowDetail[2]);

            ubigeos.push({
              departmentCode: departmentData.code,
              provinceCode: provinceData.code,
              districtCode: districtData.code,
              departamentName: departmentData.name,
              provinceName: provinceData.name,
              districtName: districtData.name
            });

          }
        };

        return ubigeos;
      }));
  }

  private getCodeName(data: string): { code: string, name: string} {
    //clean quotation marks
    data = data.replace(/"/g, "");
    let regex = /\d+/;
    let code = (data.match(regex)) ? data.match(regex)[0] : null;
    let name = (code) ? data.replace(code, "").trim() : null;
    return { code, name };
  };
}
