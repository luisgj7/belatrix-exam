export interface IUbigeo extends IDeparment, IProvince, IDistrict {
}

export interface IDeparment {
  departmentCode: string;
  departamentName: string;
}

export interface IProvince {
  provinceCode: string;
  provinceName: string;
}

export interface IDistrict {
  districtCode: string;
  districtName: string;
}